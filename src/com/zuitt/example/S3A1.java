package com.zuitt.example;

import java.util.Scanner;

public class S3A1 {
    public static void main(String[] args){
        System.out.println("Input an integer whose factorial will be computed.");

        Scanner in = new Scanner(System.in);

        try {
            int num = in.nextInt();
            int answer = 1;
            int counter = 1;

            if(num > 0){

                /*
                while(counter <= num){
                    answer *= counter;
                    counter++;
                }
                */

                for(counter = 1; counter <= num; counter++){
                    answer *= counter;
                }
                System.out.println("The factorial of " + num + " is " + answer);
            } else if(num == 0) {
                System.out.println("Factorial of 0 is 1.");
            } else {
                System.out.println("Cannot compute for factorial of negative numbers.");
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
